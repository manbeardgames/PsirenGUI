﻿namespace Psiren.GUI.Definitions
{
    /// <summary>
    /// Defines alignment vertically
    /// </summary>
    public enum VerticalAlignment
    {
        Top,
        Middle,
        Bottom
    }
}
