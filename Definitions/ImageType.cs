﻿namespace Psiren.GUI.Definitions
{
    /// <summary>
    /// Defines the type of image
    /// </summary>
    public enum ImageType
    {
        /// <summary>
        /// Simple means it's simply just the image as it is
        /// </summary>
        Simple,

        /// <summary>
        /// Sliced means the image is treated as a NineSlice
        /// </summary>
        Sliced
    }
}
