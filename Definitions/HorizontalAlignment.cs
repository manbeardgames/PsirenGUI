﻿namespace Psiren.GUI.Definitions
{
    /// <summary>
    /// Defines alignment horizontally
    /// </summary>
    public enum HorizontalAlignment
    {
        Left,
        Center,
        Right
    }
}
