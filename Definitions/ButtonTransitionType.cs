﻿namespace Psiren.GUI.Definitions
{
    /// <summary>
    /// Defines the transition of a selectable when changing states
    /// </summary>
    public enum ButtonTransitionType
    {
        /// <summary>
        /// Original color is multipled by selecteable color values
        /// </summary>
        ColorTint,

        /// <summary>
        /// Original color is replaced with selectable colo values
        /// </summary>
        ColorReplace,

        /// <summary>
        /// Texture2D is replaced. This will only work for entities that
        /// contain a UIImageComponent
        /// </summary>
        SpriteSwap
    }
}
