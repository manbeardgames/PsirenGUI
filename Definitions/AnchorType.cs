﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Psiren.GUI.Definitions
{
    public enum AnchorType
    {
        TopLeft,
        TopCenter,
        TopRight,
        

        MiddleLeft,
        MiddleCenter,
        MiddleRight,
        

        BottomLeft,
        BottomCenter,
        BottomRight,
        
        
        TopStretch,
        MiddleStretch,
        BottomStretch,
        LeftStretch,
        CenterStretch,
        RightStretch


    }
}
