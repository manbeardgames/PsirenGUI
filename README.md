# PsirenGUI
This is a quick example of the Entity Component System create for doing UI code in MonoGame. This was adapted for my needs from https://www.youtube.com/watch?v=AeCH1qiSVgo

### Requirements
The following frameworks are being used at the time of this writing:

1. MonoGame Framework 3.6.0.1625
2. MonoGame.Extended 1.0.617

The reason MonoGame.Extended is listed here as a requirement is because of the user of the Size2 class.  Size2 is used for the RectTransform2DComponent and throughout the LabelComponent.

### Description of system
This is setup so that the UI code uses an Entity Component System.  (more information on ECS at http://entity-systems.wikidot.com/).  
1. The Canvas acts as the entity manager. Currently in my MonoGame project, I have a scene management system.  Within each scene that needs to have a UI, there is a Canvas object. The Canvas contains a list of the UIEntity objects.  
2. The UIEntity objects are the entities in the ECS.  Each UIEntity object contains one or more IUIEntityComponents.  
3. The IUIEntityComponent interface describes the basic component.
4. The IUIEntityUpdateable interface describes a component that can be updated during the Update method of the entity
5. The IUIEntityDrawable interface describes a component that can be drawn during the Draw method of the entity

There are a couple of base classes that are setup to make this a bit easier to use
1. UIEntityComponentBase is a base class implementation of a component. It implements the IUIEntityComponent and IUIEntityUpdateable interfaces.  This means, by default, all components should be updateable
2. UIEntityDrawableComponentBase is a base class that inherits from UIEntityComponentBase and then implements the IUIEntityDrawable interface.

**Note: All UIEntities contain one component by default, the RectTransform2DComponent. This is so all UIEntities can at the very least have a transform description.**

### How To use
In you level or scene (or whatever you use), create a new Canvas Object
```csharp
Canvas _canvas = new Canvas();
```

Then, when you need to add a new UIEntity to the Canvas, use the .CreateNew method of the Canvas. This method will return the entity created for further user
```csharp
UIEntity label = _canvas.CreateEntity(new Vector2(400.0f, 400.0f), new Size2(100.0f, 100.0f));
```

Once you have the UIEntity created, you can start adding component to it. Ideally, you would create your own components, but included in the source here is an example component called LabelComponent.  This component describes a label of text for the UIEntity.
The following is an example of adding the component to the entity using the .AddComponent method
```csharp
label.AddComponent(new LabelComponent(label, "Hello World", font, Color.White, HorizontalAlignment.Center, VerticalAlignment.Middle));
```

Once this is completed, just be sure to call Canvas.Update() in your update method and Canvas.Draw() in your draw method


### Example usage in one of my project
The following is an example of using this in one of my projects.  
**Note, I have a scene management system created for my project, so this is where the SceneBase, transform matrix, and camera usage comes from.**
```csharp
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using Psiren.GUI;
using Psiren.GUI.Components;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;

namespace Psiren.Scenes
{
    public class TestScene : SceneBase
    {
        Canvas _canvas = new Canvas(true);
        public TestScene()
        {
        }

        public override void LoadContent()
        {
            base.LoadContent();
            BitmapFont font = Content.Load<BitmapFont>("fonts/bitstream/bitstream_32");
            Texture2D panelTex = Content.Load<Texture2D>("ui/panel_black");

            UIEntity label = _canvas.CreateEntity(new Vector2(400.0f, 400.0f), new Size2(100.0f, 100.0f));
            label.AddComponent(new LabelComponent(label, "Hello World", font, Color.White, HorizontalAlignment.Center, VerticalAlignment.Middle));

        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            _canvas.Update(gameTime);
        }

        public override void Draw(SpriteBatch sb, GameTime gt)
        {
            base.Draw(sb, gt);

            var viewMatrix = _camera.GetViewMatrix();
            sb.Begin(transformMatrix: viewMatrix);
            {
                _canvas.Draw(sb, gt);
            }
            sb.End();
        }
    }
}

```
