﻿//-----------------------------------------------------------------
//  NineSlice
//  Defines the source rectangle, position, and scale of an NineSlice
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;

namespace Psiren.GUI
{
    public class NineSlice
    {
        //-----------------------------------------------------------------
        //  Source Rectangle
        //-----------------------------------------------------------------
        #region Source Rectangle
        private Rectangle _sourceRectangle;
        public Rectangle SourceRectangle { get => _sourceRectangle; set => _sourceRectangle = value; }
        public Rectangle GetSourceRectangle() => _sourceRectangle;
        public void SetSourceRectangle(Rectangle value) => _sourceRectangle = value;
        #endregion

        //-----------------------------------------------------------------
        //  Position
        //-----------------------------------------------------------------
        #region Position
        private Vector2 _position;
        public Vector2 Position { get => _position; set => _position = value; }
        public Vector2 GetPosition() => _position;
        public void SetPosition(Vector2 value) => _position = value;
        #endregion

        //-----------------------------------------------------------------
        //  Scale
        //-----------------------------------------------------------------
        #region Scale
        private Vector2 _scale;
        public Vector2 Scale { get => _scale; set => _scale = value; }
        public Vector2 GetScale() => _scale;
        public void SetScale(Vector2 value) => _scale = value;
        #endregion  


        /// <summary>
        /// Creates a new NineSlice instance
        /// </summary>
        /// <param name="sourceRectangle">The source rectangle information</param>
        /// <param name="position">The position information</param>
        /// <param name="scale">The scale information</param>
        public NineSlice(Rectangle sourceRectangle, Vector2 position, Vector2 scale)
        {
            _sourceRectangle = sourceRectangle;
            _position = position;
            _scale = scale;
        }
    }
}
