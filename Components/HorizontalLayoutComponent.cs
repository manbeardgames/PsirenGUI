﻿using Microsoft.Xna.Framework;
using MonoGame.Extended;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Psiren.GUI.Components
{
    public class HorizontalLayoutComponent : UIEntityComponentBase
    {
        //-----------------------------------------------------------
        //  Padding
        //-----------------------------------------------------------
        private Padding _padding;
        public Padding Padding { get => _padding; set => _padding = value; }

        private Padding _margin;
        public Padding Margin { get => _margin; set => _margin = value; }

        private VerticalAlignment _elementVerticalAlignment;
        public VerticalAlignment ElementVerticalAlignment { get => _elementVerticalAlignment; set => _elementVerticalAlignment = value; }

        //-----------------------------------------------------------
        //  Initializations
        //-----------------------------------------------------------
        public HorizontalLayoutComponent(UIEntity parent)
            :base(parent)
        {
            Name = "HorizontalLayoutComponent";
            parent.ChildCollectionChanged += Parent_ChildCollectionChanged;
            var imageComponet = parent.GetComponent<UIImageComponent>();
            if(imageComponet != null)
            {
                _padding = imageComponet.Padding;
            }
            else
            {
                _padding = Padding.Zero;
            }
        }

        public HorizontalLayoutComponent(UIEntity parent, Padding padding, Padding margin, VerticalAlignment elementVAlign)
            : this(parent)
        {
            _padding = padding;
            _margin = margin;
            _elementVerticalAlignment = elementVAlign;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Start()
        {
            base.Start();
        }

        //-----------------------------------------------------------
        //  Update
        //-----------------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }

        //-----------------------------------------------------------
        //  Helper Methods
        //-----------------------------------------------------------

        //-----------------------------------------------------------
        //  EventHandler Methods
        //-----------------------------------------------------------
        private void Parent_ChildCollectionChanged(object sender, EventArgs e)
        {
            AlignChildren();
        }

        public void AlignChildren()
        {
            var maxWidth = 0.0f;
            var maxHeight = 0.0f;

            for (int i = 0; i < Parent.Children.Count; i++)
            {
                //maxWidth = Math.Max(Parent.Children[i].Transform.Size.Width, maxWidth);
                maxWidth += Parent.Children[i].Transform.Size.Width + _margin.Right;
                maxHeight = Math.Max(Parent.Children[i].Transform.Size.Height, maxHeight);
                //maxHeight += Parent.Children[i].Transform.Size.Height + _margin.Bottom;
            }
            //  To correct from additional margin added by last element
            maxHeight -= _margin.Bottom;

            float calculatedWidth = maxWidth + Padding.Left + Padding.Right;
            float calculatedHeight = maxHeight + Padding.Top + Padding.Bottom;
            //calculatedHeight = 200.0f;

            Parent.Transform.Size = new Size2(calculatedWidth, calculatedHeight);


            var imageComp = Parent.GetComponent<UIImageComponent>();
            var padding = imageComp == null ? Padding.Zero : imageComp.Padding;
            for (int i = 0; i < Parent.Children.Count; i++)
            {
                var position = Vector2.Zero;
                if (_elementVerticalAlignment == VerticalAlignment.Top)
                {
                    position = new Vector2(
                        x: i - 1 < 0 ? Parent.Transform.LocalPosition.X + padding.Left
                            : Parent.Children[i-1].Transform.LocalPosition.X + Parent.Children[i -1].Transform.Size.Width + _margin.Right,
                        y: Parent.Transform.LocalPosition.Y + padding.Top);

                    //position = new Vector2(
                    //    x: Parent.Transform.Position.X + padding.Left,
                    //    y: i - 1 < 0 ? Parent.Transform.Position.Y + padding.Top
                    //        : Parent.Children[i - 1].Transform.Position.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);

                }
                else if (_elementVerticalAlignment == VerticalAlignment.Middle)
                {
                    position = new Vector2(
                        x: i - 1 < 0 ? Parent.Transform.LocalPosition.X + padding.Left
                            : Parent.Children[i - 1].Transform.LocalPosition.X + Parent.Children[i - 1].Transform.Size.Width + _margin.Right,
                        y: Parent.Transform.LocalPosition.Y + (Parent.Transform.Size.Height / 2.0f) - (Parent.Children[i].Transform.Size.Height / 2.0f));

                    //position = new Vector2(
                    //    x: Parent.Transform.Position.X + (Parent.Transform.Size.Width / 2.0f) - (Parent.Children[i].Transform.Size.Width / 2.0f),
                    //    y: i - 1 < 0 ? Parent.Transform.Position.Y + padding.Top
                    //        : Parent.Children[i - 1].Transform.Position.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);
                }
                else if (_elementVerticalAlignment == VerticalAlignment.Bottom)
                {
                    position = new Vector2(
                        x: i - 1 < 0 ? Parent.Transform.LocalPosition.X + padding.Left
                            : Parent.Children[i - 1].Transform.LocalPosition.X + Parent.Children[i - 1].Transform.Size.Width + _margin.Right,
                        y: (Parent.Transform.LocalPosition.Y + Parent.Transform.Size.Height) - Parent.Children[i].Transform.Size.Height - padding.Bottom);

                    //position = new Vector2(
                    //    x: (Parent.Transform.Position.X + Parent.Transform.Size.Width) - Parent.Children[i].Transform.Size.Width - padding.Right,
                    //    y: i - 1 < 0 ? Parent.Transform.Position.Y + padding.Top
                    //        : Parent.Children[i - 1].Transform.Position.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);
                }
                else
                {
                    Debug.WriteLine("No Alignment???");
                }

                Parent.Children[i].Transform.LocalPosition = position;
                for (int j = 0; j < Parent.Children[i].DrawableComponents.Count; j++)
                {
                    Parent.Children[i].DrawableComponents[j].Refresh();
                }

            }
        }
    }
}
