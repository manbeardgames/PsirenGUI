﻿using Microsoft.Xna.Framework;
using Psiren.GUI.EntitySystem;
using Psiren.GUI.EntitySystem.Interfaces;
using Psiren.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Psiren.GUI.Components
{
    public class SelectableComponent : UIEntityComponentBase
    {
        //----------------------------------------------------
        //  Selected
        //----------------------------------------------------
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                if (_isSelected == value) { return; }
                _isSelected = value;
                SelectedChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private bool _canDetectInput;

        //----------------------------------------------------
        //  Up Action
        //----------------------------------------------------
        private Action _upAction;
        public Action UpAction { get => _upAction; set => _upAction = value; }

        //----------------------------------------------------
        //  Down Action
        //----------------------------------------------------
        private Action _downAction;
        public Action DownAction { get => _downAction; set => _downAction = value; }

        //----------------------------------------------------
        //  Left Action
        //----------------------------------------------------
        private Action _leftAction;
        public Action LeftAction { get => _leftAction; set => _leftAction = value; }

        //----------------------------------------------------
        //  Right Action
        //----------------------------------------------------
        private Action _rightAction;
        public Action RightAction { get => _rightAction; set => _rightAction = value; }

        //----------------------------------------------------
        //  Confirm Action
        //----------------------------------------------------
        private Action _confirmAction;
        public Action ConfirmAction { get => _confirmAction; set => _confirmAction = value; }

        //----------------------------------------------------
        //  Cancel Action
        //----------------------------------------------------
        private Action _cancelAction;
        public Action CancelAction { get => _cancelAction; set => _cancelAction = value; }

        private InputProfile _input;
        public InputProfile Input { get => _input; set => _input = value; }

        //----------------------------------------------------
        //  Selected Action
        //----------------------------------------------------
        private Action _selectedAction;
        public Action SelectedAction { get => _selectedAction; set => _selectedAction = value; }

        //----------------------------------------------------
        //  Transition Stuff
        //----------------------------------------------------
        private Color _normalColor = Color.White;
        public Color NormalColor { get => _normalColor; set => _normalColor = value; }

        private Color _selectedColor = Color.GhostWhite;
        public Color SelectedColor { get => _selectedColor; set => _selectedColor = value; }

        private Color _pressedColor = Color.WhiteSmoke;
        public Color PressedColor { get => _pressedColor; set => _pressedColor = value; }

        private Color _disabledColor = Color.Gray;
        public Color DisabledColor { get => _disabledColor; set => _disabledColor = value; }


        private List<Targetable> _targets;
        public List<Targetable> Targets { get => _targets; set => _targets = value; }


        //----------------------------------------------------
        //  Events
        //----------------------------------------------------
        private event EventHandler<EventArgs> SelectedChanged;


        //----------------------------------------------------
        //  Initializations
        //----------------------------------------------------
        private SelectableComponent(UIEntity parent)
            : base(parent)
        {
            Name = "SelectableComponent";
            _targets = new List<Targetable>();
            SelectedChanged += OnSelectedChanged;
        }

        public SelectableComponent(UIEntity parent, params IUIEntityDrawable[] targets)
            : this(parent)
        {
            for (int i = 0; i < targets.Length; i++)
            {
                Targetable targetable = new Targetable(targets[i]);
                _targets.Add(targetable);
            }
        }

        public override void Initialize()
        {
            base.Initialize();

        }

        public override void Start()
        {
            base.Start();
        }

        //----------------------------------------------------
        //  Update
        //----------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);

            if (_isSelected)
            {
                for (int i = 0; i < _targets.Count; i++)
                {
                    _targets[i].Target.Color = _selectedColor;
                }
            }
            else
            {
                for (int i = 0; i < _targets.Count; i++)
                {
                    _targets[i].Target.Color = _normalColor;
                }
            }


            if (_canDetectInput)
            {
                if (_isSelected)
                {
                    if (InputManager.CheckIfActionWasPressed(_input, InputAction.DpadUp, InputAction.LeftStickUp))
                    {
                        _upAction?.Invoke();
                    }
                    else if (InputManager.CheckIfActionWasPressed(_input, InputAction.DpadDown, InputAction.LeftStickDown))
                    {
                        _downAction?.Invoke();
                    }
                    else if (InputManager.CheckIfActionWasPressed(_input, InputAction.DpadLeft, InputAction.LeftStickLeft))
                    {
                        _leftAction?.Invoke();
                    }
                    else if (InputManager.CheckIfActionWasPressed(_input, InputAction.DpadRight, InputAction.LeftStickRight))
                    {
                        _rightAction?.Invoke();
                    }
                    else if (InputManager.CheckIfActionWasPressed(_input, InputAction.Action1))
                    {
                        _confirmAction?.Invoke();
                    }
                    else if (InputManager.CheckIfActionWasPressed(_input, InputAction.Action2))
                    {
                        _cancelAction?.Invoke();
                    }
                }
            }
            else
            {
                _canDetectInput = true;
            }
        }

        //----------------------------------------------------
        //  AddTarget Methods
        //----------------------------------------------------
        public bool AddTarget<T>(T target) where T : IUIEntityDrawable
        {
            if (target == null)
            {
                throw new ArgumentNullException("Target can not be null");
            }
            else
            {
                Targetable targetable = new Targetable(target);
                if (_targets.Contains(targetable)) { return false; }
                _targets.Add(targetable);
                return true;
            }
        }

        public bool RemoveTarget<T>(T target) where T : IUIEntityDrawable
        {
            if (target == null)
            {
                throw new ArgumentNullException("Target can not be null");
            }
            else
            {
                Targetable targetable = new Targetable(target);
                if (_targets.Contains(targetable))
                {
                    _targets.Remove(targetable);
                    return true;
                }
                return false;
            }
        }


        //----------------------------------------------------
        //  EventHandler Methods
        //----------------------------------------------------
        private void OnSelectedChanged(object sender, EventArgs e)
        {
            _canDetectInput = false;
            if(_isSelected)
            {
                _selectedAction?.Invoke();
            }
        }


    }
}
