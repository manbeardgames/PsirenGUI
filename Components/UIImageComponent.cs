﻿using Microsoft.Win32.SafeHandles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;
using System.Runtime.InteropServices;

namespace Psiren.GUI.Components
{
    public class UIImageComponent : UIEntityDrawableComponentBase
    {
        //----------------------------------------------------
        //  Texture
        //----------------------------------------------------
        private Texture2D _texture;
        public Texture2D Texture { get => _texture; set => _texture = value; }
        public Texture2D GetTexture() => _texture;
        public void SetTexture(Texture2D value) => _texture = value;


        //----------------------------------------------------
        //  ImageType
        //----------------------------------------------------
        private ImageType _imageType = ImageType.Sliced;
        public ImageType ImageType
        {
            get { return _imageType; }
            set
            {
                if (_imageType == value) { return; }
                _imageType = value;
                ImageTypeChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public ImageType GetImageType() => _imageType;
        public void SetImageType(ImageType value)
        {
            if (_imageType == value) { return; }
            _imageType = value;
            ImageTypeChanged?.Invoke(this, EventArgs.Empty);
        }

        //----------------------------------------------------
        //  NineSlice collection
        //----------------------------------------------------
        private NineSlice[] _nineSlices;

        //----------------------------------------------------
        //  NineSlice Padding
        //----------------------------------------------------
        private Padding _padding = Padding.Zero;
        public Padding Padding { get => _padding;
            set
            {
                if (_padding == value) { return; }
                _padding = value;
                GenerateNineSlice();
            }
        }


        //----------------------------------------------------
        //  Events
        //----------------------------------------------------
        public event EventHandler<EventArgs> ImageTypeChanged;

        //----------------------------------------------------
        //  Initializations
        //----------------------------------------------------
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="parent">The UIEntity this belongs to</param>
        public UIImageComponent(UIEntity parent, Texture2D texture) : base(parent)
        {
            Name = "UIImageComponent";
            _texture = texture;
            ImageTypeChanged += OnImageTypeChanged;
            parent.Transform.SizeChanged += OnParentSizeChange;
        }


        public override void Initialize()
        {
            base.Initialize();
            if(_imageType == ImageType.Sliced)
            {
                GenerateNineSlice();
            }

        }

        public override void Start()
        {
            base.Start();
        }

        //----------------------------------------------------
        //  Update
        //----------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }

        //----------------------------------------------------
        //  Draw
        //----------------------------------------------------
        public override void Draw(SpriteBatch sb, GameTime gt)
        {
            base.Draw(sb, gt);

            if(_texture == null)
            {
                sb.FillRectangle(
                    location: Parent.Transform.GetAdditivePosition(),
                    size: Parent.Transform.Size,
                    color: Color);
            }
            else if (_imageType == ImageType.Simple)
            {
                sb.Draw(
                    texture: _texture,
                    position: Parent.Transform.LocalPosition,
                    sourceRectangle: null,
                    color: Color,
                    rotation: Parent.Transform.Rotation,
                    origin: Vector2.Zero,
                    scale: Vector2.One,
                    effects: SpriteEffects.None,
                    layerDepth: 0.0f);
            }
            else
            {
                for (int i = 0; i < _nineSlices.Length; i++)
                {
                    sb.Draw(
                        texture: _texture,
                        position: _nineSlices[i].Position,
                        sourceRectangle: _nineSlices[i].SourceRectangle,
                        color: Color,
                        rotation: Parent.Transform.Rotation,
                        origin: Vector2.Zero,
                        scale: _nineSlices[i].Scale,
                        effects: SpriteEffects.None,
                        layerDepth: 0.0f);
                }
            }
        }

        public override void DrawDebug(SpriteBatch sb, GameTime gt)
        {
        }

        //----------------------------------------------------
        //  Helper Methods
        //----------------------------------------------------
        public override void Refresh()
        {
            if (_imageType == ImageType.Sliced)
            {
                GenerateNineSlice();
            }
            
        }

        private void GenerateNineSlice()
        {
            if (_texture == null) { return; }
            _nineSlices = new NineSlice[9];
            

            //-------------------------- TOP LEFT ----------------------------------//
            Rectangle topLeftRect = new Rectangle(0, 0, _padding.Left, _padding.Top);
            Vector2 topLeftPos = new Vector2(Parent.Transform.LocalPosition.X, Parent.Transform.LocalPosition.Y);
            Vector2 topLeftScale = new Vector2(1, 1);
            _nineSlices[0] = new NineSlice(topLeftRect, topLeftPos, topLeftScale);

            //-------------------------- TOP MID ----------------------------------//
            Rectangle topMidRect = new Rectangle(_padding.Left, 0, _texture.Width - (_padding.Left + _padding.Right), _padding.Top);
            Vector2 topMidPos = new Vector2(Parent.Transform.LocalPosition.X + _padding.Left, Parent.Transform.LocalPosition.Y);
            Vector2 topMidScale = new Vector2((Parent.Transform.Size.Width - (_padding.Left + _padding.Right)) / topMidRect.Width, 1);
            _nineSlices[1] = new NineSlice(topMidRect, topMidPos, topMidScale);

            //-------------------------- TOP RIGHT --------------------------------//
            Rectangle topRightRect = new Rectangle(_texture.Width - _padding.Right, 0, _padding.Right, _padding.Top);
            Vector2 topRightPos = new Vector2(Parent.Transform.LocalPosition.X + Parent.Transform.Size.Width - _padding.Right, Parent.Transform.LocalPosition.Y);
            Vector2 topRightScale = new Vector2(1, 1);
            _nineSlices[2] = new NineSlice(topRightRect, topRightPos, topRightScale);

            //-------------------------- CENTER LEFT ------------------------------//
            Rectangle centerLeftRect = new Rectangle(0, _padding.Top, _padding.Left, _texture.Height - _padding.Top - _padding.Bottom);
            Vector2 centerLeftPos = new Vector2(Parent.Transform.LocalPosition.X, Parent.Transform.LocalPosition.Y + _padding.Top);
            Vector2 centerLeftScale = new Vector2(1, (Parent.Transform.Size.Height - _padding.Top - _padding.Bottom) / centerLeftRect.Height);
            _nineSlices[3] = new NineSlice(centerLeftRect, centerLeftPos, centerLeftScale);

            //-------------------------- CENTER MID -------------------------------//
            Rectangle centerMidRect = new Rectangle(_padding.Left, _padding.Top, _texture.Width - _padding.Left - _padding.Right, _texture.Height - _padding.Top - _padding.Bottom);
            Vector2 centerMidPos = new Vector2(Parent.Transform.LocalPosition.X + _padding.Left, Parent.Transform.LocalPosition.Y + _padding.Top);
            Vector2 centerMidScale = new Vector2((Parent.Transform.Size.Width - _padding.Left - _padding.Right) / centerMidRect.Width, (Parent.Transform.Size.Height - _padding.Top - _padding.Bottom) / centerMidRect.Height);
            _nineSlices[4] = new NineSlice(centerMidRect, centerMidPos, centerMidScale);

            //-------------------------- CENTER RIGHT -----------------------------//
            Rectangle centerRightRect = new Rectangle(_texture.Width - _padding.Right, _padding.Top, _padding.Right, _texture.Height - _padding.Top - _padding.Bottom);
            Vector2 centerRightPos = new Vector2(Parent.Transform.LocalPosition.X + Parent.Transform.Size.Width - _padding.Right, Parent.Transform.LocalPosition.Y + _padding.Top);
            Vector2 centerRightScale = new Vector2(1, (Parent.Transform.Size.Height - _padding.Top - _padding.Bottom) / centerRightRect.Height);
            _nineSlices[5] = new NineSlice(centerRightRect, centerRightPos, centerRightScale);

            //-------------------------- BOTTOM LEFT ------------------------------//
            Rectangle bottomLeftRect = new Rectangle(0, _texture.Height - _padding.Bottom, _padding.Left, _padding.Bottom);
            Vector2 bottomLeftPos = new Vector2(Parent.Transform.LocalPosition.X, Parent.Transform.LocalPosition.Y + Parent.Transform.Size.Height - _padding.Bottom);
            Vector2 bottomLeftScale = new Vector2(1, 1);
            _nineSlices[6] = new NineSlice(bottomLeftRect, bottomLeftPos, bottomLeftScale);

            //-------------------------- BOTTOM MID -------------------------------//
            Rectangle bottomMidRect = new Rectangle(_padding.Left, _texture.Height - _padding.Bottom, _texture.Width - _padding.Left - _padding.Right, _padding.Bottom);
            Vector2 bottomMidPos = new Vector2(Parent.Transform.LocalPosition.X + _padding.Left, Parent.Transform.LocalPosition.Y + Parent.Transform.Size.Height - _padding.Bottom);
            Vector2 bottomMidScale = new Vector2((Parent.Transform.Size.Width - _padding.Left - _padding.Right) / bottomMidRect.Width, 1);
            _nineSlices[7] = new NineSlice(bottomMidRect, bottomMidPos, bottomMidScale);

            //-------------------------- BOTTOM RIGHT -----------------------------//
            Rectangle bottomRightRect = new Rectangle(_texture.Width - _padding.Right, _texture.Height - _padding.Bottom, _padding.Right, _padding.Bottom);
            Vector2 bottomRightPos = new Vector2(Parent.Transform.LocalPosition.X + Parent.Transform.Size.Width - _padding.Right, Parent.Transform.LocalPosition.Y + Parent.Transform.Size.Height - _padding.Bottom);
            Vector2 bottomRightScale = new Vector2(1, 1);
            _nineSlices[8] = new NineSlice(bottomRightRect, bottomRightPos, bottomRightScale);

        }

        //----------------------------------------------------
        //  EventHandler Methods
        //----------------------------------------------------
        private void OnImageTypeChanged(object sender, EventArgs e)
        {
            if(_imageType == ImageType.Sliced)
            {
                GenerateNineSlice();
            }
        }

        private void OnParentSizeChange(object sender, EventArgs e)
        {
            if (_imageType == ImageType.Sliced)
            {
                GenerateNineSlice();
            }
        }
    }
}
