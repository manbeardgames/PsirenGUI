﻿using Psiren.GUI.EntitySystem;
using System;
using System.Collections.Generic;
using System.Text;

namespace Psiren.GUI.Components
{
    public class LayoutDefinitionComponent : UIEntityComponentBase
    {
        private float _minimumWidth = float.MinValue;
        public float MinimumWidth { get => _minimumWidth; set => _minimumWidth = value; }

        private float _maximumWidth = float.MaxValue;
        public float MaximumWidth { get => _maximumWidth; set => _maximumWidth = value; }

        private float _minimumHeight = float.MinValue;
        public float MinimumHeight { get => _minimumHeight; set => _minimumHeight = value; }

        private float _maximumHeight = float.MaxValue;
        public float MaximumHeight { get => _maximumHeight; set => _maximumHeight = value; }



        public LayoutDefinitionComponent(UIEntity parent)
            : base(parent)
        {
            Name = "LayoutDefinitionComponent";
        }

    }
}
