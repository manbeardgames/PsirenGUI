﻿using Psiren.GUI.EntitySystem;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Psiren.GUI.Definitions;
using MonoGame.Extended;
using System.Diagnostics;

namespace Psiren.GUI.Components
{
    public class ContentFitterComponent : UIEntityComponentBase
    {
        private AutoSizeType _verticalFit = AutoSizeType.Fitted;
        public AutoSizeType VerticalFit { get => _verticalFit; set => _verticalFit = value; }

        private AutoSizeType _horizontalFit = AutoSizeType.Fitted;
        public AutoSizeType HorizontalFit { get => _horizontalFit; set => _horizontalFit = value; }

        public ContentFitterComponent(UIEntity parent)
            :base(parent)
        {
            Name = "ContentFitterComponent";
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Start()
        {
            base.Start();
        }

        public override void Update(GameTime gt)
        {
            base.Update(gt);
            List<UIEntity> tempEntities = new List<UIEntity>();
            tempEntities.AddRange(Parent.Children);
            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);
            for (int i = 0; i < tempEntities.Count; i++)
            {
                RectangleF bounds = new RectangleF(
                    x: tempEntities[i].Transform.GetAdditivePosition().X,
                    y: tempEntities[i].Transform.GetAdditivePosition().Y,
                    width: tempEntities[i].Transform.Size.Width,
                    height: tempEntities[i].Transform.Size.Height);

                min.X = Math.Min(bounds.X, min.X);
                min.Y = Math.Min(bounds.Y, min.Y);
                max.X = Math.Max(bounds.X + bounds.Width, max.X);
                max.Y = Math.Max(bounds.Y + bounds.Height, max.Y);
            }


            Size2 newSize = Parent.Transform.Size;
            if(_horizontalFit == AutoSizeType.Fitted)
            {
                newSize.Height = max.Y - min.Y;
            }
            if(_verticalFit == AutoSizeType.Fitted)
            {
                newSize.Width = max.X - min.X;
            }
            Debug.WriteLine($"Min: {min} -- Max: {max} -- NewSize: {newSize}");
            Parent.Transform.Size = newSize;
        }
    }
}
