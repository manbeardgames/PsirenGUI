﻿using MonoGame.Extended;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Psiren.Utilities;

namespace Psiren.GUI.Components
{
    public class GridLayoutComponent : UIEntityComponentBase
    {
        //-----------------------------------------------------------
        //  Padding
        //  Defaults to Padding.Zero
        //-----------------------------------------------------------
        private Padding _padding = Padding.Zero;
        public Padding Padding { get => _padding; set => _padding = value; }

        //-----------------------------------------------------------
        //  CellSize
        //  Defaults to 100, 100
        //-----------------------------------------------------------
        private Size2 _cellSize = new Size2(100, 100);
        public Size2 CellSize { get => _cellSize; set => _cellSize = value; }

        //-----------------------------------------------------------
        //  Spacing
        //  Defaults to 0, 0
        //-----------------------------------------------------------
        private Size2 _spacing = new Size2(0, 0);
        public Size2 Spacing { get => _spacing; set => _spacing = value; }

        //-----------------------------------------------------------
        //  Alignment
        //  Defaults to TopLeft
        //-----------------------------------------------------------
        private Alignment _alignment = Alignment.TopLeft;
        public Alignment Alignment { get => _alignment; set => _alignment = value; }


        //-----------------------------------------------------------
        //  Initializations
        //-----------------------------------------------------------
        public GridLayoutComponent(UIEntity parent) : base(parent)
        {
            Name = "GridLayoutComponent";
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Start()
        {
            base.Start();
        }

        //-----------------------------------------------------------
        //  Update
        //-----------------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);
            List<UIEntity> tempEntities = new List<UIEntity>();
            tempEntities.AddRange(Parent.Children);
            for (int i = 0; i < tempEntities.Count; i++)
            {
                var position = GridUtilities.GetGridPosition(i, Parent.Transform.Size, _cellSize);
                var pposition = Parent.Transform.GetAdditivePosition();
                tempEntities[i].Transform.LocalPosition = position + new Vector2(Padding.Left, Padding.Top);
                tempEntities[i].Transform.Size = _cellSize;
            }
        }
    }
}
