﻿using Microsoft.Xna.Framework;
using MonoGame.Extended;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;
using System.Diagnostics;

namespace Psiren.GUI.Components
{
    class VerticalLayoutComponent : UIEntityComponentBase
    {
        //-----------------------------------------------------------
        //  Padding
        //-----------------------------------------------------------
        private Padding _padding;
        public Padding Padding { get => _padding; set => _padding = value; }

        private Padding _margin;
        public Padding Margin { get => _margin; set => _margin = value; }

        private HorizontalAlignment _elementHorizontalAlignment;
        public HorizontalAlignment ElementHorizontalAlignmnet { get => _elementHorizontalAlignment; set => _elementHorizontalAlignment = value; }




        //-----------------------------------------------------------
        //  Initializations
        //-----------------------------------------------------------
        public VerticalLayoutComponent(UIEntity parent)
            : base(parent)
        {
            Name = "VerticalLayoutComponent";
            parent.ChildCollectionChanged += Parent_ChildCollectionChanged;
            var imageComponent = parent.GetComponent<UIImageComponent>();
            if (imageComponent != null)
            {
                _padding = imageComponent.Padding;
            }
            else
            {
                _padding = Padding.Zero;
            }
            _margin = Padding.Zero;
        }

        public VerticalLayoutComponent(UIEntity parent, Padding padding, Padding margin, HorizontalAlignment elementHAlign)
            : this(parent)
        {
            _padding = padding;
            _margin = margin;
            _elementHorizontalAlignment = elementHAlign;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Start()
        {
            base.Start();
        }

        //-----------------------------------------------------------
        //  Update
        //-----------------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }

        //-----------------------------------------------------------
        //  Helper Methods
        //-----------------------------------------------------------

        //-----------------------------------------------------------
        //  EventHandler Methods
        //-----------------------------------------------------------
        private void Parent_ChildCollectionChanged(object sender, EventArgs e)
        {
            AlignChildren();
        }

        public void AlignChildren()
        {
            var maxWidth = 0.0f;
            var maxHeight = 0.0f;

            for (int i = 0; i < Parent.Children.Count; i++)
            {
                maxWidth = Math.Max(Parent.Children[i].Transform.Size.Width, maxWidth);
                maxHeight += Parent.Children[i].Transform.Size.Height + _margin.Bottom;
            }
            //  To correct from additional margin added by last element
            maxHeight -= _margin.Bottom;

            float calculatedWidth = maxWidth + Padding.Left + Padding.Right;
            float calculatedHeight = maxHeight + Padding.Top + Padding.Bottom;

            Parent.Transform.Size = new Size2(calculatedWidth, calculatedHeight);


            var imageComp = Parent.GetComponent<UIImageComponent>();
            var padding = imageComp == null ? Padding.Zero : imageComp.Padding;
            for (int i = 0; i < Parent.Children.Count; i++)
            {
                var position = Vector2.Zero;
                if (_elementHorizontalAlignment == HorizontalAlignment.Left)
                {
                    position = new Vector2(
                        x: Parent.Transform.LocalPosition.X + padding.Left,
                        y: i - 1 < 0 ? Parent.Transform.LocalPosition.Y + padding.Top
                            : Parent.Children[i - 1].Transform.LocalPosition.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);

                }
                else if (_elementHorizontalAlignment == HorizontalAlignment.Center)
                {
                    position = new Vector2(
                        x: Parent.Transform.LocalPosition.X + (Parent.Transform.Size.Width / 2.0f) - (Parent.Children[i].Transform.Size.Width / 2.0f),
                        y: i - 1 < 0 ? Parent.Transform.LocalPosition.Y + padding.Top
                            : Parent.Children[i - 1].Transform.LocalPosition.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);
                }
                else if (_elementHorizontalAlignment == HorizontalAlignment.Right)
                {
                    position = new Vector2(
                        x: (Parent.Transform.LocalPosition.X + Parent.Transform.Size.Width) - Parent.Children[i].Transform.Size.Width - padding.Right,
                        y: i - 1 < 0 ? Parent.Transform.LocalPosition.Y + padding.Top
                            : Parent.Children[i - 1].Transform.LocalPosition.Y + Parent.Children[i - 1].Transform.Size.Height + _margin.Bottom);
                }
                else
                {
                    Debug.WriteLine("No Alignment???");
                }

                Parent.Children[i].Transform.LocalPosition = position;
                for (int j = 0; j < Parent.Children[i].DrawableComponents.Count; j++)
                {
                    Parent.Children[i].DrawableComponents[j].Refresh();
                }

            }
        }
    }
}
