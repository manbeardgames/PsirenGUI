﻿using MonoGame.Extended.BitmapFonts;
using Psiren.GUI.EntitySystem;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;

namespace Psiren.GUI.Components
{
    public class UIMessageBoxComponent : UIEntityDrawableComponentBase
    {
        //----------------------------------------------------
        //  Fields
        //----------------------------------------------------
        private List<string> _lines = new List<string>();

        //----------------------------------------------------
        //  Text
        //----------------------------------------------------
        private string _text = "";
        public string Text
        {
            get => _text;
            set
            {
                if (_text == value) { return; }
                _text = value;
                TextChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        //----------------------------------------------------
        //  Font
        //----------------------------------------------------
        private BitmapFont _font;
        public BitmapFont Font
        {
            get => _font;
            set
            {
                if (_font == value) { return; }
                _font = value;
                FontChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private float _lineHeight = 1.0f;
        public float LineHeight
        {
            get => _lineHeight;
            set
            {
                if (_lineHeight == value) { return; }
                _lineHeight = value;
                LineHeightChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        //----------------------------------------------------
        //  Events
        //----------------------------------------------------
        public event EventHandler<EventArgs> TextChanged;
        public event EventHandler<EventArgs> FontChanged;
        public event EventHandler<EventArgs> LineHeightChanged;

        //----------------------------------------------------
        //  Initializations
        //----------------------------------------------------
        public UIMessageBoxComponent(UIEntity parent, BitmapFont font)
            : base(parent)
        {
            Name = "UIMessageBoxComponent";
            _font = font;
            TextChanged += OnTextChanged;
            FontChanged += OnFontChanged;
            LineHeightChanged += OnLineHeightChanged;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Start()
        {
            base.Start();
            Refresh();
        }

        //----------------------------------------------------
        //  Udpate
        //----------------------------------------------------
        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }

        //----------------------------------------------------
        //  Draw
        //----------------------------------------------------
        public override void Draw(SpriteBatch sb, GameTime gt)
        {
            base.Draw(sb, gt);
            var pposition = Parent.Transform.GetAdditivePosition();
            for (int i = 0; i < _lines.Count; i++)
            {
                sb.DrawString(
                    bitmapFont: _font,
                    text: _lines[i],
                    position: new Vector2(
                        x: pposition.X,
                        y: pposition.Y + (_lineHeight * i)),
                    color: Color,
                    rotation: Parent.Transform.Rotation,
                    origin: Vector2.Zero,
                    scale: Vector2.One,
                    effect: SpriteEffects.None,
                    clippingRectangle: new Rectangle(pposition.ToPoint(), new Point((int)Parent.Transform.Size.Width, (int)Parent.Transform.Size.Height)),
                    layerDepth: 0.0f);
            }
        }

        //----------------------------------------------------
        //  Helper Methods
        //----------------------------------------------------
        public override void Refresh()
        {
            base.Refresh();
            _lines.Clear();
            float width = Parent.Transform.Size.Width;

            string line = "";
            for (int i = 0; i < _text.Length; i++)
            {
                float length = _font.MeasureString(line + _text[i]).Width;
                if(length < width)
                {
                    line += _text[i];
                }
                else
                {
                    //  Before adding the line, ensure we are no clipping a full word
                    if (i + 1 < _text.Length)
                    {
                        if (_text[i - 1] == ' ')
                        {
                            _lines.Add(line);
                            line = "";
                            //  Increment i so we dont' add a space to the beginning of the
                            //  next line
                            i++;
                            line += _text[i];
                        }
                        else
                        {
                            _lines.Add(line.Substring(0, line.LastIndexOf(' ')));
                            line = line.Substring(line.LastIndexOf(' ')+1);
                            i--;

                        }
                    }

                }
            }
            if(line.Length > 0)
            {
                _lines.Add(line);
            }
        }

        //----------------------------------------------------
        //  EventHandler Methods
        //----------------------------------------------------
        private void OnFontChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void OnTextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void OnLineHeightChanged(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
