﻿//-----------------------------------------------------------------
//  UIEntityComponentBase
//  Base class which defines a UIEntity component. All UIEntity
//  components should inherit from this class if they are not
//  drawable
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.GUI.EntitySystem;
using Psiren.GUI.EntitySystem.Interfaces;
using System;

namespace Psiren.GUI.Components
{
    public class UIEntityComponentBase : IUIEntityComponent, IUIEntityUpdateable
    {
        //-------------------------------------------------
        //  Name
        //-------------------------------------------------
        #region Name
        private string _name;
        public string Name
        {
            get { return _name; }
            protected set { _name = value; }
        }
        public string GetName() => _name;
        protected void SetName(string value) => _name = value;
        #endregion

        //-------------------------------------------------
        //  Enabled
        //-------------------------------------------------
        #region Enabled
        private bool _enabled = true;
        public bool Enabled
        {
            get { return _enabled; }
            set
            {
                if (_enabled == value) return;
                _enabled = value;
                EnabledChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public bool GetEnabled() => _enabled;
        public void SetEnabled(bool value)
        {
            if (_enabled == value) return;
            _enabled = value;
            EnabledChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  UpdateOrder
        //-------------------------------------------------
        #region UpdateOrder
        private int _updateOrder = 0;
        public int UpdateOrder
        {
            get { return _updateOrder; }
            set
            {
                if (_updateOrder == value) return;
                _updateOrder = value;
                UpdateOrderChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public int GetUpdateOrder() => _updateOrder;
        public void SetUpdateOrder(int value)
        {
            if (_updateOrder == value) return;
            _updateOrder = value;
            UpdateOrderChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  Parent
        //-------------------------------------------------
        #region Parent
        private readonly UIEntity _parent;
        public UIEntity Parent { get => _parent; }
        public UIEntity GetParent() => _parent;

        #endregion

        //-------------------------------------------------
        //  Events
        //-------------------------------------------------
        #region Events
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;
        #endregion

        //-------------------------------------------------
        //  Initializations
        //-------------------------------------------------
        #region Initializations
        /// <summary>
        /// Creates a new insteance of UIEntityComponentBase
        /// </summary>
        /// <param name="parent"></param>
        public UIEntityComponentBase(UIEntity parent)
        {
            _parent = parent;
            EnabledChanged += OnEnabledChanged;
            UpdateOrderChanged += OnUpdateOrderChanged;
        }

        /// <summary>
        /// Called at when the component is added to an entity
        /// </summary>
        public virtual void Initialize() { }

        /// <summary>
        /// Called after initialize.  Use to get reference to other components
        /// in parent entity
        /// </summary>
        public virtual void Start() { }
        #endregion

        //-------------------------------------------------
        //  Update
        //-------------------------------------------------
        #region Update
        /// <summary>
        /// Udpates the component
        /// </summary>
        /// <param name="gt"></param>
        public virtual void Update(GameTime gt) { }
        #endregion

        //-------------------------------------------------
        //  EventHandler Methods
        //-------------------------------------------------
        #region EventHandler Methods
        /// <summary>
        /// Called when the Enabled property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnEnabledChanged(object sender, EventArgs e) { }

        /// <summary>
        /// Called when the UpdateOrder property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnUpdateOrderChanged(object sender, EventArgs e) { }
        #endregion  



    }
}
