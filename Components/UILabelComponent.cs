﻿//-----------------------------------------------------------------
//  LabelComponent
//  Defines a label component including Text, Font, Color, 
//  vertical alignment, and horizontal alignment
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;

namespace Psiren.GUI.Components
{

    public class UILabelComponent : UIEntityDrawableComponentBase
    {
        //-------------------------------------------------
        //  Text
        //-------------------------------------------------
        #region Text
        private string _text = string.Empty;
        public string Text
        {
            get { return _text; }
            set
            {
                if (_text == value) { return; }
                _text = value;
                TextChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public string GetText() => _text;
        public void SetText(string value)
        {
            if (_text == value) { return; }
            _text = value;
            TextChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  Font
        //-------------------------------------------------
        #region Font
        private BitmapFont _font;
        public BitmapFont Font
        {
            get { return _font; }
            set
            {
                if (_font == value) { return; }
                _font = value;
                FontChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public BitmapFont GetFont() => _font;
        public void SetFont(BitmapFont value)
        {
            if (_font == value) { return; }
            _font = value;
            FontChanged?.Invoke(this, EventArgs.Empty);

        }
        #endregion  

        //-------------------------------------------------
        //  Vertical Alignment
        //-------------------------------------------------
        #region Vertical Alignment
        private VerticalAlignment _verticalAlignment;
        public VerticalAlignment VerticalAlignment
        {
            get { return _verticalAlignment; }
            set
            {
                if (_verticalAlignment == value) return;
                _verticalAlignment = value;
                AlignmentChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public VerticalAlignment GetVerticalAlignment() => _verticalAlignment;
        public void SetVerticalAlignment(VerticalAlignment value)
        {
            if (_verticalAlignment == value) return;
            _verticalAlignment = value;
            AlignmentChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion 

        //-------------------------------------------------
        //  Horizontal Alignment
        //-------------------------------------------------
        #region Horizontal Alignment
        private HorizontalAlignment _horizontalAlignment;
        public HorizontalAlignment HorizontalAlignment
        {
            get { return _horizontalAlignment; }
            set
            {
                if (_horizontalAlignment == value) return;
                _horizontalAlignment = value;
                AlignmentChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public HorizontalAlignment GetHorizontalAlignment() => _horizontalAlignment;
        public void SetHorizontalAlignment(HorizontalAlignment value)
        {
            if (_horizontalAlignment == value) return;
            _horizontalAlignment = value;
            AlignmentChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        private Vector2 _scale = Vector2.One;
        public Vector2 Scale { get => _scale; }

        private bool _bestFit;
        public bool BestFit
        {
            get => _bestFit;
            set
            {
                if (_bestFit == value) { return; }
                _bestFit = value;
                BestFitChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        //-------------------------------------------------
        //  Events
        //-------------------------------------------------
        #region Events
        public event EventHandler<EventArgs> TextChanged;
        public event EventHandler<EventArgs> FontChanged;
        public event EventHandler<EventArgs> AlignmentChanged;
        public event EventHandler<EventArgs> BestFitChanged;

        #endregion

        //-------------------------------------------------
        //  Initializations
        //-------------------------------------------------
        #region Initializations
        /// <summary>
        /// Creates a new insteance of LabelComponent
        /// </summary>
        /// <param name="parent">The UIEntity parent this component is attached to</param>
        public UILabelComponent(UIEntity parent, BitmapFont font) : base(parent)
        {
            Name = "LabelComponent";
            _font = font;
            TextChanged += OnTextChanged;
            FontChanged += OnFontChanged;
            AlignmentChanged += OnAlignmentChanged;
            BestFitChanged += OnBestFitChanged;
        }

        /// <summary>
        /// Called when component is created.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Called after Initialize.
        /// </summary>
        public override void Start()
        {
            base.Start();
            Refresh();
        }
        #endregion

        //-------------------------------------------------
        //  Update
        //-------------------------------------------------
        #region Update
        /// <summary>
        /// Updates the label
        /// </summary>
        /// <param name="gt"></param>
        public override void Update(GameTime gt)
        {
            base.Update(gt);
        }
        #endregion

        //-------------------------------------------------
        //  Draw
        //-------------------------------------------------
        #region Draw
        /// <summary>
        /// Draws the label
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public override void Draw(SpriteBatch sb, GameTime gt)
        {
            base.Draw(sb, gt);
            var pposition = Parent.Transform.GetAdditivePosition();
            sb.DrawString(
                bitmapFont: _font,
                text: _text,
                position: pposition + _drawOffset,
                color: Color,
                rotation: Parent.Transform.Rotation,
                origin: Vector2.Zero,
                scale: _scale,
                effect: SpriteEffects.None,
                clippingRectangle: new Rectangle(pposition.ToPoint(), new Point((int)Parent.Transform.Size.Width, (int)Parent.Transform.Size.Height)),
                layerDepth: 0.0f);

        }

        public override void DrawDebug(SpriteBatch sb, GameTime gt)
        {
            base.DrawDebug(sb, gt);
            var pposition = Parent.Transform.GetAdditivePosition();
            var textSize = _font.MeasureString(_text) * _scale.X;
            sb.DrawRectangle(
                rectangle: new RectangleF((pposition + _drawOffset).ToPoint(), textSize),
                color: Color.Green);
            sb.DrawLine(
                point1: new Vector2((pposition + _drawOffset).X + (textSize.Width / 2.0f), (pposition + _drawOffset).Y),
                point2: new Vector2((pposition + _drawOffset).X + (textSize.Width / 2.0f), (pposition + _drawOffset).Y + textSize.Height),
                color: Color.Yellow,
                thickness: 2.0f);

            sb.DrawLine(
                point1: new Vector2((pposition + _drawOffset).X , (pposition + _drawOffset).Y + (textSize.Height / 2.0f)),
                point2: new Vector2((pposition + _drawOffset).X + textSize.Width, (pposition + _drawOffset).Y + (textSize.Height / 2.0f)),
                color: Color.Yellow,
                thickness: 2.0f);
        }
        #endregion

        
        //-------------------------------------------------
        //  Helper Methods
        //-------------------------------------------------
        #region Helper Methods
        /// <summary>
        /// Sets the alignment of the text based on the vertical and
        /// horizontal alignment property
        /// </summary>
        public override void Refresh()
        {
            //  Gather needed information
            var parentRect = Parent.Transform;
            var parentPos = Parent.Transform.GetAdditivePosition();
            var parentSize = Parent.Transform.Size;
            var parentCenterPos = new Vector2(parentPos.X + (parentSize.Width / 2.0f), parentPos.Y + (parentSize.Height / 2.0f));

            var textMeasurement = _font.MeasureString(_text);
            var textCenter = textMeasurement / 2.0f;

            var paddingComponent = Parent.GetComponent<UIImageComponent>();
            var padding = paddingComponent == null ? Padding.Zero : paddingComponent.Padding;

            Vector2 offset = Vector2.Zero;
            Vector2 scale = Vector2.One;

            switch (_verticalAlignment)
            {
                case VerticalAlignment.Top:
                    offset.Y = padding.Top;
                    break;
                case VerticalAlignment.Middle:
                    offset.Y = ((parentSize.Height/* - padding.Top - padding.Bottom*/) / 2.0f) - textCenter.Height;
                    break;
                case VerticalAlignment.Bottom:
                    offset.Y = parentSize.Height - padding.Bottom - textMeasurement.Height;
                    break;
                default:
                    offset.Y = padding.Top;
                    break;
            }

            switch (_horizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    offset.X = padding.Left;
                    break;
                case HorizontalAlignment.Center:
                    offset.X = ((parentSize.Width /*- padding.Left - padding.Right*/) / 2.0f) - textCenter.Width;
                    break;
                case HorizontalAlignment.Right:
                    offset.X = parentSize.Width - padding.Right - textMeasurement.Width;
                    break;
                default:
                    break;
            }


            _drawOffset = offset;
            _scale = scale;

        }
        #endregion

        //-------------------------------------------------
        //  EventHandler Methods
        //-------------------------------------------------
        #region EventHandler Methods
        /// <summary>
        /// Called when the Text property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnTextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Called when the Font property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnFontChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Called when the Vertical or Horizontal alignment is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAlignmentChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void OnBestFitChanged(object sender, EventArgs e)
        {
        }

        #endregion
    }
}
