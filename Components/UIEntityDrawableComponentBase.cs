﻿//-----------------------------------------------------------------
//  UIEntityDrawableComponentBase
//  Base class which defines a UIEntity component that is
//  drawable. For ease of use, any custom components you
//  make that are drawable should inherit this class
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Psiren.GUI.EntitySystem;
using Psiren.GUI.EntitySystem.Interfaces;
using System;

namespace Psiren.GUI.Components
{
    public class UIEntityDrawableComponentBase : UIEntityComponentBase, IUIEntityDrawable
    {
        //-------------------------------------------------
        //  Visible
        //-------------------------------------------------
        #region Visible
        private bool _visible = true;
        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (_visible == value) { return; }
                _visible = value;
                VisibleChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public bool GetVisible() => _visible;
        public void SetVisible(bool value)
        {
            if (_visible == value) { return; }
            _visible = value;
            VisibleChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  DrawOrder
        //-------------------------------------------------
        #region DrawOrder
        private int _drawOrder = 0;
        public int DrawOrder
        {
            get { return _drawOrder; }
            set
            {
                if (_drawOrder == value) { return; }
                _drawOrder = value;
                DrawOrderChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public int GetDrawOrder() => _drawOrder;
        public void SetDrawOrder(int value)
        {
            if (_drawOrder == value) { return; }
            _drawOrder = value;
            DrawOrderChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  DrawOffset
        //-------------------------------------------------
        protected Vector2 _drawOffset = Vector2.Zero;
        public Vector2 DrawOffset { get => _drawOffset; protected set => _drawOffset = value; }

        

        //-------------------------------------------------
        //  Color
        //-------------------------------------------------
        private Color _color = Color.White;
        public Color Color { get => _color; set => _color = value; }
        public Color GetColor() => _color;
        public void SetColor(Color value) => _color = value;



        //-------------------------------------------------
        //  Events
        //-------------------------------------------------
        #region Events
        public event EventHandler<EventArgs> VisibleChanged;
        public event EventHandler<EventArgs> DrawOrderChanged;
        #endregion

        //-------------------------------------------------
        //  Initializations
        //-------------------------------------------------
        #region Initializations
        /// <summary>
        /// Creates a new instance of a UIEntityDrawableComponent
        /// </summary>
        /// <param name="parent"></param>
        public UIEntityDrawableComponentBase(UIEntity parent) : base(parent)
        {
            VisibleChanged += OnVisibleChanged;
            DrawOrderChanged += OnDrawOrderChanged;
        }
        #endregion

        //-------------------------------------------------
        //  Draw
        //-------------------------------------------------
        #region Draw
        /// <summary>
        /// Draws the component
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public virtual void Draw(SpriteBatch sb, GameTime gt) { }

        /// <summary>
        /// Draws the components debug
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public virtual void DrawDebug(SpriteBatch sb, GameTime gt) { }
        #endregion


        public virtual void Refresh() { }



        //-------------------------------------------------
        //  EventHandler Methods
        //-------------------------------------------------
        #region EventHandler Methods
        /// <summary>
        /// Called when the Visible property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnVisibleChanged(object sender, EventArgs e) { }

        /// <summary>
        /// Called when the DrawOrder property is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void OnDrawOrderChanged(object sender, EventArgs e) { }
        #endregion


    }
}
