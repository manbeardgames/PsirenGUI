﻿//-----------------------------------------------------------------
//  RectTransform2DComponent
//  Defines a rectangular transform including position and size
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using MonoGame.Extended;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem;
using System;
using System.Diagnostics;

namespace Psiren.GUI.Components
{
    public class RectTransform2DComponent : UIEntityComponentBase
    {

        //-------------------------------------------------
        //  AnchorType
        //-------------------------------------------------
        private AnchorType _anchor = AnchorType.TopLeft;
        public AnchorType Anchor { get => _anchor; set => _anchor = value; }


        //-------------------------------------------------
        //  Position
        //-------------------------------------------------
        #region Position
        private Vector2 _localPosition = Vector2.Zero;
        public Vector2 LocalPosition
        {
            get { return _localPosition; }
            set
            {
                if (_localPosition == value) { return; }
                _localPosition = value;
                PositionChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public Vector2 GetPosition() => _localPosition;
        public void SetPosition(Vector2 value)
        {
            if (_localPosition == value) { return; }
            _localPosition = value;
            PositionChanged?.Invoke(this, EventArgs.Empty);
        }

        public Vector2 GetAdditivePosition()
        {
            UIEntity parentEntity = Parent;
            if(parentEntity.Parent == null)
            {
                return _localPosition;
            }
            else
            {
                
                return parentEntity.Parent.Transform.GetAdditivePosition() + _localPosition;
            }

        }
        #endregion

        //-------------------------------------------------
        //  Size
        //-------------------------------------------------
        #region Size
        private Size2 _size = new Size2(100.0f, 100.0f);
        public Size2 Size
        {
            get { return _size; }
            set
            {
                if (_size == value) { return; }
                _size = value;
                SizeChanged?.Invoke(this, EventArgs.Empty);
            }
        }
        public Size2 GetSize() => _size;
        public void SetSize(Size2 value)
        {
            if (_size == value) { return; }
            _size = value;
            SizeChanged?.Invoke(this, EventArgs.Empty);
        }
        #endregion

        //-------------------------------------------------
        //  Rotation
        //-------------------------------------------------
        #region Rotation
        public float _rotation = 0.0f;
        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }
        public float GetRotation() => _rotation;
        public void SetRotation(float value) => _rotation = value;
        #endregion

        //-------------------------------------------------
        //  Events
        //-------------------------------------------------
        public event EventHandler<EventArgs> PositionChanged;
        public event EventHandler<EventArgs> SizeChanged;

        //-------------------------------------------------
        //  Initializations
        //-------------------------------------------------
        #region Initializations
        /// <summary>
        /// Creates a new instance of the RectTransform2DComponent
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="position"></param>
        /// <param name="size"></param>
        public RectTransform2DComponent(UIEntity parent) : base(parent)
        {
            Name = "RectTransform2DComponent";
        }
        #endregion

        public void StretchToParent()
        {
            _localPosition = Vector2.Zero;
            UIEntity myEntityParent = Parent;
            _size = myEntityParent.Parent != null ? myEntityParent.Parent.Transform.Size : _size;
        }

        /// <summary>
        /// TODO: Figure out what the hell do do with this
        /// </summary>
        /// <param name="anchor"></param>
        public void SetAnchor(AnchorType anchor)
        {
            if (_anchor == anchor) { return; }
            _anchor = anchor;

            switch (_anchor)
            {
                case AnchorType.TopLeft:
                    break;
                case AnchorType.TopCenter:
                    break;
                case AnchorType.TopRight:
                    break;
                case AnchorType.MiddleLeft:
                    break;
                case AnchorType.MiddleCenter:
                    break;
                case AnchorType.MiddleRight:
                    break;
                case AnchorType.BottomLeft:
                    break;
                case AnchorType.BottomCenter:
                    break;
                case AnchorType.BottomRight:
                    break;
                case AnchorType.TopStretch:
                    break;
                case AnchorType.MiddleStretch:
                    break;
                case AnchorType.BottomStretch:
                    break;
                case AnchorType.LeftStretch:
                    break;
                case AnchorType.CenterStretch:
                    break;
                case AnchorType.RightStretch:
                    break;
                default:
                    break;
            }

        }

    }
}
