﻿//-----------------------------------------------------------------
//  IUIEntityComponent
//  Interface which defines the contract for any UIEntity component.
//  All UIEntity compatible components must implemtn this interface
//-----------------------------------------------------------------
namespace Psiren.GUI.EntitySystem.Interfaces
{
    public interface IUIEntityComponent
    {
        /// <summary>
        /// The name of the component
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Used to initialize the component 
        /// </summary>
        /// <remarks>
        /// Initialize is called before Start
        /// </remarks>
        void Initialize();

        /// <summary>
        /// Called after initialize. Gather references to other components in the entity here
        /// </summary>
        void Start();
    }
}
