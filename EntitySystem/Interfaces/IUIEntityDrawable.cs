﻿//-----------------------------------------------------------------
//  IUIEntityDrawable
//  Interface which defines the contract for any UIEntity component
//  that is drawable.
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Psiren.GUI.EntitySystem.Interfaces
{
    public interface IUIEntityDrawable
    {
        /// <summary>
        /// Gets whether the component should be drawn
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// Gets the order in which the component should be drawn
        /// Components with a smaller value are drawn first
        /// </summary>
        int DrawOrder { get; }

        Vector2 DrawOffset { get; }

        Color Color { get; set; }


        /// <summary>
        /// INvoked when the Visible property changes
        /// </summary>
        event EventHandler<EventArgs> VisibleChanged;

        /// <summary>
        /// Invoked when the DrawOrder property changes
        /// </summary>
        event EventHandler<EventArgs> DrawOrderChanged;

        void Refresh();

        /// <summary>
        /// Draws the component
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        void Draw(SpriteBatch sb, GameTime gt);

        /// <summary>
        /// Draws the components debug
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        void DrawDebug(SpriteBatch sb, GameTime gt);
    }
}
