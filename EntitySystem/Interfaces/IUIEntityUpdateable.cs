﻿//-----------------------------------------------------------------
//  IUIEntityUpdateable
//  Interface which defines the contract for any UIEntity component
//  that is updateable.
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using System;

namespace Psiren.GUI.EntitySystem.Interfaces
{
    public interface IUIEntityUpdateable
    {
        /// <summary>
        /// Gets whether the component should be updated
        /// </summary>
        bool Enabled { get; }

        /// <summary>
        /// Gets the order in which the component should be udpated
        /// Components with a smaller value are updated first
        /// </summary>
        int UpdateOrder { get; }

        /// <summary>
        /// INvoked when the Enabled property changes
        /// </summary>
        event EventHandler<EventArgs> EnabledChanged;

        /// <summary>
        /// Invoked when the UdpateOrder property changes
        /// </summary>
        event EventHandler<EventArgs> UpdateOrderChanged;

        /// <summary>
        /// Updates the component
        /// </summary>
        /// <param name="gt"></param>
        void Update(GameTime gt);
    }
}
