﻿//-----------------------------------------------------------------
//  UIEntity
//  Defines a new UIEntity object.  UIEntitys contain components
//  of type IUIEntityComponent.  
//-----------------------------------------------------------------


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using Psiren.GUI.Components;
using Psiren.GUI.Definitions;
using Psiren.GUI.EntitySystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Psiren.GUI.EntitySystem
{
    public class UIEntity
    {
        private string _name = string.Empty;
        public string Name { get => _name; }
        //------------------------------------------------------------------
        //  All component collections
        //------------------------------------------------------------------
        #region All component collections
        /// <summary>
        /// Contains a list of all IUIEntityComponents
        /// </summary>
        private List<IUIEntityComponent> _components = new List<IUIEntityComponent>();

        /// <summary>
        /// Dirtly list of all components
        /// </summary>
        private List<IUIEntityComponent> _dirtyComponents = new List<IUIEntityComponent>();

        /// <summary>
        /// Dictionary of all components that serves as a lookup by name container
        /// </summary>
        public Dictionary<string, IUIEntityComponent> Components = new Dictionary<string, IUIEntityComponent>();
        #endregion


        //------------------------------------------------------------------
        //  Updateable component collections
        //------------------------------------------------------------------
        #region Updateable component collection
        /// <summary>
        /// Contains a list of all components that are updateable
        /// </summary>
        private List<IUIEntityUpdateable> _updateableComponents = new List<IUIEntityUpdateable>();
        public List<IUIEntityUpdateable> UpdateableComponents { get => _updateableComponents; }

        /// <summary>
        /// Dirty list of all updateable componentss
        /// </summary>
        private List<IUIEntityUpdateable> _dirtyUpdateableComponents = new List<IUIEntityUpdateable>();
        #endregion


        //------------------------------------------------------------------
        //  Drawable component collections
        //------------------------------------------------------------------
        #region Drawable Component Collections
        /// <summary>
        /// Contains a list of all compoentns that are drawable
        /// </summary>
        private List<IUIEntityDrawable> _drawableComponents = new List<IUIEntityDrawable>();
        public List<IUIEntityDrawable> DrawableComponents { get => _drawableComponents; }

        /// <summary>
        /// Dirty list of all drawable components
        /// </summary>
        private List<IUIEntityDrawable> _dirtyDrawableComponents = new List<IUIEntityDrawable>();
        #endregion



        //-----------------------------------------------------------
        //  Parent Canvas
        //-----------------------------------------------------------
        #region Parent Canvas
        //private Canvas _parentCanvas;
        //public Canvas ParentCanvas { get => _parentCanvas; }
        //public Canvas GetParentCanvas() => _parentCanvas;
        #endregion

        //-----------------------------------------------------------
        //  Parent Entity
        //-----------------------------------------------------------
        private UIEntity _parent;
        public UIEntity Parent { get => _parent; set => _parent = value; }

        //-----------------------------------------------------------
        //  Child Entites
        //-----------------------------------------------------------
        private List<UIEntity> _children = new List<UIEntity>();
        public List<UIEntity> Children { get => _children; }

        //-----------------------------------------------------------
        //  Properties
        //-----------------------------------------------------------
        #region Properties
        public int Count { get => _components.Count; }
        public int GetCount => _components.Count;
        #endregion

        //-----------------------------------------------------------
        //  RectTransform2DComponent
        //-----------------------------------------------------------
        #region RectTransform2DComponent
        protected RectTransform2DComponent _transform;
        public RectTransform2DComponent Transform { get => _transform; set => _transform = value; }
        public RectTransform2DComponent GetTransform() => _transform;
        public void SetTransform(RectTransform2DComponent value) => _transform = value;
        #endregion


        //-----------------------------------------------------------
        //  Fields
        //-----------------------------------------------------------
        #region Fields
        /// <summary>
        /// Has this entity been initialized
        /// </summary>
        private bool _isInitialized = false;
        #endregion


        //-----------------------------------------------------------
        //  Events
        //-----------------------------------------------------------
        public event EventHandler<EventArgs> ChildCollectionChanged;


        //-----------------------------------------------------------
        //  Initialization
        //-----------------------------------------------------------
        #region Initializations
        /// <summary>
        /// Constructor for entity
        /// </summary>
        public UIEntity(UIEntity parentElement = null, string name = "")
        {
            _parent = parentElement;
            if(_parent != null)
            {
                _parent.AddChild(this);
            }
            _name = name;


            //  Always give an entity a transform
            Transform = AddComponent(new RectTransform2DComponent(this));
            Transform.PositionChanged += Transform_PositionChanged;
            Transform.SizeChanged += Transform_SizeChanged;
        }

        private void Transform_SizeChanged(object sender, EventArgs e)
        {
            _dirtyDrawableComponents.Clear();
            _dirtyDrawableComponents.AddRange(_drawableComponents);
            for (int i = 0; i < _dirtyDrawableComponents.Count; i++)
            {
                _dirtyDrawableComponents[i].Refresh();
            }
        }

        private void Transform_PositionChanged(object sender, EventArgs e)
        {
            _dirtyDrawableComponents.Clear();
            _dirtyDrawableComponents.AddRange(_drawableComponents);
            for(int i = 0; i < _dirtyDrawableComponents.Count; i++)
            {
                _dirtyDrawableComponents[i].Refresh();
            }
        }

        /// <summary>
        /// Initializes the entity
        /// </summary>
        public virtual void Initialize()
        {
            //  Ensure initialize is only run once
            if (_isInitialized) { return; }

            _dirtyComponents.Clear();
            _dirtyComponents.AddRange(_components);

            for (int i = 0; i < _dirtyComponents.Count; i++)
            {
                _dirtyComponents[i].Initialize();
            }

            for (int i = 0; i < _dirtyComponents.Count; i++)
            {
                _dirtyComponents[i].Start();
            }

            _isInitialized = true;
        }
        #endregion

        //-----------------------------------------------------------
        //  Update
        //-----------------------------------------------------------
        #region Update
        /// <summary>
        /// Updates the entity and all its updateable components
        /// </summary>
        /// <param name="gt"></param>
        public virtual void Update(GameTime gt)
        {
            _dirtyUpdateableComponents.Clear();
            _dirtyUpdateableComponents.AddRange(_updateableComponents);

            for (int i = 0; i < _dirtyUpdateableComponents.Count; i++)
            {
                if (_dirtyUpdateableComponents[i].Enabled)
                {
                    _dirtyUpdateableComponents[i].Update(gt);
                }


            }
        }
        #endregion

        //-----------------------------------------------------------
        //  Draw
        //-----------------------------------------------------------
        #region Draw
        /// <summary>
        /// Draws the entity and all its drawable components
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public virtual void Draw(SpriteBatch sb, GameTime gt)
        {
            _dirtyDrawableComponents.Clear();
            _dirtyDrawableComponents.AddRange(_drawableComponents);

            for (int i = 0; i < _dirtyDrawableComponents.Count; i++)
            {
                if (_dirtyDrawableComponents[i].Visible)
                {
                    _dirtyDrawableComponents[i].Draw(sb, gt);
                }
            }
        }

        /// <summary>
        /// Draws the entities debug and all its drawable components DrawDebug
        /// commands
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public void DrawDebug(SpriteBatch sb, GameTime gt)
        {
            _dirtyDrawableComponents.Clear();
            _dirtyDrawableComponents.AddRange(_drawableComponents);

            for (int i = 0; i < _dirtyDrawableComponents.Count; i++)
            {
                if (_dirtyDrawableComponents[i].Visible)
                {
                    _dirtyDrawableComponents[i].DrawDebug(sb, gt);
                }
            }

            var paddingComponent = GetComponent<UIImageComponent>();
            var padding = paddingComponent == null ? Padding.Zero : paddingComponent.Padding;
            var position = Transform.GetAdditivePosition();

            sb.DrawRectangle(
                rectangle: new RectangleF(position.X, position.Y, _transform.Size.Width, _transform.Size.Height),
                color: Color.Green);

            sb.DrawLine(
                point1: new Vector2(position.X + (_transform.Size.Width / 2.0f), position.Y),
                point2: new Vector2(position.X + (_transform.Size.Width / 2.0f), position.Y + _transform.Size.Height),
                color: Color.Red,
                thickness: 1.0f);

            sb.DrawLine(
                point1: new Vector2(position.X, position.Y + (_transform.Size.Height / 2.0f)),
                point2: new Vector2(position.X + _transform.Size.Width, position.Y + (_transform.Size.Height / 2.0f)),
                color: Color.Red,
                thickness: 1.0f);
        }
        #endregion

        //-----------------------------------------------------------
        //  Adding and Removing Components
        //-----------------------------------------------------------
        #region Adding and Removing Components
        /// <summary>
        /// Adds a new component to this entity where the component is of
        /// base type UIEntityComponentBase
        /// </summary>
        /// <typeparam name="T">The component type to add. Must be of base type UIEntityComponentBase</typeparam>
        /// <param name="component">The component to add</param>
        /// <returns>The component</returns>
        public T AddComponent<T>(T component) where T : UIEntityComponentBase
        {
            //  Ensure componeont is given
            if (component == null) { throw new ArgumentNullException("Component is null"); }

            //  Ensure we don't already have one of this type of component
            if (_components.Contains(component)) { return component; }

            //  Add to master and lookup list
            _components.Add(component);
            Components.Add(component.Name, component);

            IUIEntityUpdateable updateable = component as IUIEntityUpdateable;
            IUIEntityDrawable drawable = component as IUIEntityDrawable;
            

            //  If the component can be udpated add it to the update list
            if (updateable != null)
            {
                _updateableComponents.Add(updateable);
                updateable.UpdateOrderChanged += OnComponentUpdateOrderChanged;
                OnComponentUpdateOrderChanged(this, EventArgs.Empty);
            }

            //  IF the componnet is drawable, add it to the draw list
            if (drawable != null)
            {
                _drawableComponents.Add(drawable);
                drawable.DrawOrderChanged += OnComponentDrawOrderChanged;
                OnComponentDrawOrderChanged(this, EventArgs.Empty);
            }

            //  If this entity has already been initialized, call the components initialize and start methods
            if (_isInitialized)
            {
                component.Initialize();
                component.Start();
            }

            return component;
        }

        /// <summary>
        /// Removes the given component from this entity
        /// </summary>
        /// <param name="component">The component to remove</param>
        /// <returns>True if the remove was successful, false otherwise</returns>
        public bool RemoveComponent(IUIEntityComponent component)
        {
            //  Ensure the component given isn't null
            if (component == null) { throw new ArgumentNullException("Component was null"); }

            if (_components.Remove(component))
            {
                IUIEntityUpdateable updateable = component as IUIEntityUpdateable;
                IUIEntityDrawable drawable = component as IUIEntityDrawable;
                

                if (updateable != null)
                {
                    _updateableComponents.Remove(updateable);
                    updateable.UpdateOrderChanged -= OnComponentUpdateOrderChanged;
                }

                if (drawable != null)
                {
                    _drawableComponents.Remove(drawable);
                    drawable.DrawOrderChanged -= OnComponentDrawOrderChanged;
                }



                return true;
            }
            return false;
        }
        #endregion

        //-----------------------------------------------------------
        //  Adding and Removing Children
        //-----------------------------------------------------------
        public bool AddChild(UIEntity child)
        {
            if (_children.Contains(child)) { return false; }
            _children.Add(child);
            ChildCollectionChanged?.Invoke(this, EventArgs.Empty);
            return true;
        }

        public bool RemoveChild(UIEntity child)
        {
            if(_children.Contains(child))
            {
                _children.Remove(child);
                ChildCollectionChanged?.Invoke(this, EventArgs.Empty);
                return true;
            }

            return false;
        }

        //-----------------------------------------------------------
        //  Helper Methods
        //-----------------------------------------------------------
        #region Helper Methods
        public void RefreshDrawables()
        {
            for(int i = 0; i < _drawableComponents.Count; i++)
            {
                _drawableComponents[i].Refresh();
            }

            for(int i = 0; i < _children.Count; i++)
            {
                _children[i].RefreshDrawables();
            }
        }
        
        /// <summary>
        /// Attempts to get a component attached to this entity by name
        /// </summary>
        /// <param name="name">The name of the component to get</param>
        /// <returns>The component</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Will throw exception if no component is found with the name given</exception>
        public IUIEntityComponent GetComponent(string name)
        {
            if (Components.ContainsKey(name))
            {
                return Components[name] as IUIEntityComponent;
            }
            else
            {
                throw new ArgumentOutOfRangeException(name);
            }
        }

        /// <summary>
        /// Gets the component of type T that is attached to this UIEntity
        /// </summary>
        /// <typeparam name="T">The type of component where T is IUIEntityComponent</typeparam>
        /// <returns>The component if found, null otherwise</returns>
        public T GetComponent<T>() where T : IUIEntityComponent
        {
            var component = Components.FirstOrDefault(c => c.Value is T);
            return (T)component.Value;
        }

        /// <summary>
        /// Called when the update order of components is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnComponentUpdateOrderChanged(object sender, EventArgs e)
        {
            _updateableComponents.Sort(UpdateableSort);
        }

        /// <summary>
        /// Helper method for sorting updateable componentss
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static int UpdateableSort(IUIEntityUpdateable a, IUIEntityUpdateable b)
        {
            return a.UpdateOrder.CompareTo(b.UpdateOrder);
        }


        /// <summary>
        /// Called when the draw order of components is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnComponentDrawOrderChanged(object sender, EventArgs e)
        {
            _drawableComponents.Sort(DrawableSort);
        }

        /// <summary>
        /// Helper method for sorting drawable components
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static int DrawableSort(IUIEntityDrawable a, IUIEntityDrawable b)
        {
            return a.DrawOrder.CompareTo(b.DrawOrder);
        }
        #endregion

    }
}
