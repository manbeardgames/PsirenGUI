﻿//-----------------------------------------------------------------
//  Padding
//  Defines the amount of padding to use when creating a NineSlice
//-----------------------------------------------------------------

namespace Psiren.GUI
{
    public class Padding
    {
        //-----------------------------------------------------------------
        //  Used to get quick Padding types with preset values
        //-----------------------------------------------------------------
        public static readonly Padding Zero = new Padding(0);
        public static readonly Padding One = new Padding(1);
        public static readonly Padding Two = new Padding(2);
        public static readonly Padding Three = new Padding(3);
        public static readonly Padding Four = new Padding(4);
        public static readonly Padding Five = new Padding(5);
        public static readonly Padding Six = new Padding(6);
        public static readonly Padding Seven = new Padding(7);


        //-----------------------------------------------------------------
        //  Padding Top, Bottom, Left, and Right properties
        //-----------------------------------------------------------------
        public int Top { get; set; }
        public int Bottom { get; set; }
        public int Left { get; set; }
        public int Right { get; set; }


        //-----------------------------------------------------------------
        //  Constructors
        //-----------------------------------------------------------------
        /// <summary>
        /// Creates a Padding instenace
        /// </summary>
        /// <param name="top">The amount of top padding</param>
        /// <param name="bottom">The amount of bottom padding</param>
        /// <param name="left">The amount of left padding</param>
        /// <param name="right">The amount of right padding</param>
        public Padding(int top, int bottom, int left, int right)
        {
            Top = top;
            Bottom = bottom;
            Left = left;
            Right = right;
        }

        /// <summary>
        /// Creates a new Padding instance where the top and bottom are equal
        /// and the left and right are equal
        /// </summary>
        /// <param name="topAndBottom">The amount of padding for the top and bottom</param>
        /// <param name="leftAndRight">The amount of padding for the left and right</param>
        public Padding(int topAndBottom, int leftAndRight)
            : this(topAndBottom, topAndBottom, leftAndRight, leftAndRight) { }


        /// <summary>
        /// Creats a new Padding instance where all sides are equal amounts
        /// </summary>
        /// <param name="topAndBottomAndLeftAndRight">The amount of padding equal for all side</param>
        public Padding(int topAndBottomAndLeftAndRight)
            : this(topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight, topAndBottomAndLeftAndRight) { }  
    }
}
