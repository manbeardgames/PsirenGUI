﻿//-----------------------------------------------------------------
//  Targetable
//  Defines a targetable component for the SelectableComponent  
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Psiren.GUI.EntitySystem.Interfaces;

namespace Psiren.GUI
{
    public class Targetable
    {
        //-----------------------------------------------------------------
        //  The UIEntityDrawable that should be targeted
        //-----------------------------------------------------------------
        private IUIEntityDrawable _target;
        public IUIEntityDrawable Target { get => _target; }


        //-----------------------------------------------------------------
        //  The original color of the UIEntityDrawable.
        //-----------------------------------------------------------------
        private Color _originalColor;
        public Color OriginalColor { get => _originalColor; }

        public Targetable(IUIEntityDrawable target)
        {
            _target = target;
            _originalColor = target.Color;
        }

    }
}
