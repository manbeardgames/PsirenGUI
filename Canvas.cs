﻿//-----------------------------------------------------------------
//  Canvas
//  Defines a new Canvs
//  The Canvas is a container of UIEntity objects.  The Container
//  acts as a manger for the UIEntities
//-----------------------------------------------------------------
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using Psiren.GUI.EntitySystem;
using System.Collections.Generic;

namespace Psiren.GUI
{
    public class Canvas : UIEntity
    {
        //------------------------------------------------------------------
        //  Entity Collection
        //------------------------------------------------------------------
        #region Entity Colleciton
        private List<UIEntity> _entities = new List<UIEntity>();
        public List<UIEntity> Entities { get => _entities; }
        public List<UIEntity> GetEntities() => _entities;

        private List<UIEntity> _dirtyEntities = new List<UIEntity>();
        #endregion

        //------------------------------------------------------------------
        //  Debug Mode
        //------------------------------------------------------------------
        #region Debug Mode
        private bool _debugMode;
        public bool DebugMode { get => _debugMode; set => _debugMode = value; }
        public bool GetDebugMode() => _debugMode;
        public void SetDebugMode(bool value) => _debugMode = value;
        #endregion

        //------------------------------------------------------------------
        //  Initializations
        //------------------------------------------------------------------
        #region Initializations
        /// <summary>
        /// Default emtpy constructor
        /// </summary>
        /// <param name="debugMode">Should canvas operate in debug mode</param>
        public Canvas(bool debugMode = false) : base(null)
        {
            _debugMode = debugMode;
        }

        /// <summary>
        /// Creates a new Canvas object with the given list of UIEntities
        /// </summary>
        /// <param name="entities">Initial list of UIEntities for the Canvas</param>
        /// <param name="debugMode">Should Canvas operate in debug mode</param>
        public Canvas(List<UIEntity> entities, bool debugMode = false)
            : this(debugMode)
        {
            _entities = entities;
        }

        /// <summary>
        /// Initializes the Canvas and all its entities
        /// </summary>
        public override void Initialize()
        {
            _dirtyEntities.Clear();
            _dirtyEntities.AddRange(_entities);
            for (int i = 0; i < _dirtyEntities.Count; i++)
            {
                _dirtyEntities[i].Initialize();
            }
        }
        #endregion

        //------------------------------------------------------------------
        //  Update
        //------------------------------------------------------------------
        #region Update
        /// <summary>
        /// Updates the Canvas and all its entities
        /// </summary>
        /// <param name="gt"></param>
        public override void Update(GameTime gt)
        {
            _dirtyEntities.Clear();
            _dirtyEntities.AddRange(_entities);
            for (int i = 0; i < _dirtyEntities.Count; i++)
            {
                _dirtyEntities[i].Update(gt);
            }
        }
        #endregion

        //------------------------------------------------------------------
        //  Draw
        //------------------------------------------------------------------
        #region Draw
        /// <summary>
        /// Draws the canvas and all its entities
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="gt"></param>
        public override void Draw(SpriteBatch sb, GameTime gt)
        {
            _dirtyEntities.Clear();
            _dirtyEntities.AddRange(_entities);
            for (int i = 0; i < _dirtyEntities.Count; i++)
            {
                _dirtyEntities[i].Draw(sb, gt);
                if(_debugMode)
                {
                    _dirtyEntities[i].DrawDebug(sb, gt);
                }
            }

        }
        #endregion

        //------------------------------------------------------------------
        //  Helper Methods
        //------------------------------------------------------------------
        #region Helper Methods
        /// <summary>
        /// Creates a nwe UIEntity and adds it to the entity list of 
        /// this Canvs
        /// </summary>
        /// <param name="position">The position of the entity</param>
        /// <param name="size">The size of the entity</param>
        /// <returns></returns>
        public UIEntity CreateEntity(UIEntity parent = null, string name = "")
        {
            UIEntity entity = new UIEntity(parent, name);
            _entities.Add(entity);
            entity.Initialize();
            return entity;
        }

        /// <summary>
        /// Clears the Canvas of all entities
        /// </summary>
        public void Clear()
        {
            _entities.Clear();
            _dirtyEntities.Clear();
        }

        
        #endregion



    }
}
